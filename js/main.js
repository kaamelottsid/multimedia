$(document).ready(function() {
    // on instancie l'application
    var app = new Application({
        element: $("#kaamelott"),
        hasher: hasher,
        crossroads: crossroads
    });

    /* Les scènes */
    sceneOne(app);
    sceneTwo(app);
    sceneThree(app);
    sceneFour(app);
    sceneFive(app);
    sceneSix(app);
    sceneSeven(app);
    sceneEight(app);
    sceneNine(app);
    sceneTen(app);
});