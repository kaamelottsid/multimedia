/** Quelques constantes **/
const RATIO_WIDTH = 0.6;
const RATIO_HEIGHT = 0.8;
const TOP_MARGIN = 20;

const DEFAULT_DELAY = 1500;

/**
 * Constructeur de l'application
 * @param {Object} les dépendances pour le constructeur
 */
function Application(opts) {
    this.$container = opts.element;
    this.hasher = opts.hasher;
    this.crossroads = opts.crossroads;

    this.init();
}

/**
 * Initialisation du livre
 * @return {Application} l'instance de l'application
 */
Application.prototype.init = function() {
    var self = this;

    /* Menu */
    var $menu = $(".menu"),
        $menuIndices = $menu.find(".indices");

    $menu.find(".menu-tooltip").on('click', function() {
        if($menuIndices.is(':visible')) {
            $menuIndices.fadeOut();
        } else {
            $menuIndices.fadeIn();
        }
    });

    /* Sons */
    createjs.Sound.registerSound("assets/sounds/knock-knock.mp3", "KNOCK-KNOCK");
    createjs.Sound.registerSound("assets/sounds/fire.mp3", "FIRE");

    self.$container.css({
        'top': TOP_MARGIN + 'px',
        'left': getMargin() + 'px'
    });

    self.$container.turn({
        width: getWidth(),
        height: getHeight(),
        autoCenter: true,
        shadows: $.isTouch,
        acceleration: $.isTouch
    });

    $(window).resize(function() {
        self.$container.css({
            'left': getMargin() + 'px'
        });

        self.$container.turn('size', getWidth(), getHeight());
    });

    self.$container.bind("turned", function(e, page) {
        self.redirect('/pages/' + page);
        self.hooks(page);
    });

    self.crossroads.addRoute('/pages/{nb}', function(nb) {
        var currentPage = self.$container.turn('page');

        if(nb != currentPage) {
            try {
                self.$container.turn('page', nb);
            } catch(e) {
                // Fallback dans le cas où on consulte le livre depuis une URL directement
                setTimeout(function() {
                    self.$container.turn('page', nb);
                }, 0);
            }
        }
    });

    var parseHash = function(newHash, oldHash) {
        self.crossroads.parse(newHash);
    };

    self.hasher.initialized.add(parseHash);
    self.hasher.changed.add(parseHash);
    self.hasher.init();

    return this;
};

/**
 * Permet de définir une page du livre
 * @param  {Number}                        number   le numéro de la page
 * @param  {String | jQuery DOM Element}   content  le contenu de la page
 * @param  {Function}                      callback (optionnel) une action qui peut s'effectuer au moment où on découvre la page
 * @return {Application}                   l'instance de l'application
 */
Application.prototype.definePage = function(number, content, callback) {
    var self = this;
    var pageCount = self.$container.turn('pages')+1;

    self.$container.turn('addPage', content, number);
    self.$container.turn('pages', pageCount);

    if(callback) {
        self.$container.bind('turned', function(e, page, view) {
            if(view.indexOf(number) != -1) callback();
        });
    }

    return this;
};

/**
 * Permet de changer l'URL dans le navigateur
 * @param  {String} route une route
 * @return {Application} l'instance de l'application
 */
Application.prototype.redirect = function(route) {
    this.hasher.setHash(route.substring(1));
    return this;
};

/**
 * Permet de tourner la page courante vers la page suivante
 * @return {Application} l'instance de l'application
 */
Application.prototype.turnToNextPage = function() {
    this.$container.turn("next");
    return this;
};

/**
 * Contient toutes les actions se déclenchant à partir
 * d'un certain numéro de page
 * @param  {Number} nb le numéro de la page courante
 */
Application.prototype.hooks = function(nb) {
    var $avatar = $('.avatar-container'),
        $indices = $(".menu").find(".indices").find("ul");

    var $indiceApple = $('#indice-apple'),
        $indiceGobelins = $('#indice-gobelins');

    if(nb >= 6) {
        if(!$avatar.hasClass('filled')) $avatar.addClass('filled');
    }

    if(nb >= 12) {
        if($indiceApple.length == 0) $indices.html('<li id="indice-apple"><a href="#/pages/13">Perceval disait que le vieil homme n\'arrêtait pas de dire "Apple".</a></li>');
    }

    if(nb >= 16) {
        if($indiceGobelins.length == 0) $indices.append('<li id="indice-gobelins"><a href="#/pages/17">Dans la bibliothèque, on pouvait lire sur la couveture d\'un des livres : "Les gobelins craignent le feu".</a></li>');
    }
};

/**
 * Permet de dessiner une bulle de dialogue
 * @param  {CanvasRenderingContext2D} context contexte du canvas
 * @param  {String} content contenu de la bulle
 * @param  {Number} w       largeur de la bulle
 * @param  {Number} x       abscisse du point central de la bulle
 * @param  {Number} y       ordonnée du point central de la bulle
 * @param  {Number} px      abscisse du point de provenance de la bulle
 * @param  {Number} py      ordonnée du point de provenance de la bulle
 * @param  {Number}  h      hauteur de la bulle (optionnel)
 * @return {Application}    l'instance de l'application
 */
Application.prototype.drawBubble = function(context, content, w, x, y, px, py, h) {
    var radius = 20;

    h = h || 70;

    var r = x + w;
    var b = y + h;

    if(py < y || py > y+h){
        var con1 = Math.min(Math.max(x+radius,px-10),r-radius-20);
        var con2 = Math.min(Math.max(x+radius+20,px+10),r-radius);
    } else {
        var con1 = Math.min(Math.max(y+radius,py-10),b-radius-20);
        var con2 = Math.min(Math.max(y+radius+20,py+10),b-radius);
    }

    var dir;

    if(py < y) dir = 2;
    if(py > y) dir = 3;
    if(px < x && py>=y && py<=b) dir = 0;
    if(px > x && py>=y && py<=b) dir = 1;
    if(px >= x && px <= r && py >= y && py <= b) dir = -1;

    context.beginPath();
    context.lineWidth = 2;
    context.moveTo(x+radius,y);

    if(dir==2){
        context.lineTo(con1,y);
        context.lineTo(px,py);
        context.lineTo(con2,y);
        context.lineTo(r-radius,y);
    } else context.lineTo(r-radius,y);

    context.quadraticCurveTo(r,y,r,y+radius);

    if(dir==1){
        context.lineTo(r,con1);
        context.lineTo(px,py);
        context.lineTo(r,con2);
        context.lineTo(r,b-radius);
    } else context.lineTo(r,b-radius);

    context.quadraticCurveTo(r, b, r-radius, b);

    if(dir==3){
        context.lineTo(con2,b);
        context.lineTo(px,py);
        context.lineTo(con1,b);
        context.lineTo(x+radius,b);
    } else context.lineTo(x+radius,b);

    context.quadraticCurveTo(x, b, x, b-radius);

    if(dir==0){
        context.lineTo(x,con2);
        context.lineTo(px,py);
        context.lineTo(x,con1);
        context.lineTo(x,y+radius);
    } else context.lineTo(x,y+radius);

    context.quadraticCurveTo(x, y, x+radius, y);

    // intérieur de la bulle
    context.fillStyle = "white";
    context.fill();

    // contour de la bulle
    context.strokeStyle = "black";
    context.stroke();

    // contenu textuel de la bulle
    context.font = "normal 1.12em Comic Sans MS";
    context.wrapText(content, x+10, y+40, w, 40);

    return this;
};

/**
 * Méthode ajoutée au contexte 2D d'un canvas qui permet
 * d'avoir du texte sur plusieurs lignes
 * @param  {String} text        contenu textuel
 * @param  {Number} x          abscisse du point du début du texte
 * @param  {Number} y          ordonnée du point du début du texte
 * @param  {Number} maxWidth   largeur maximale du texte
 * @param  {Number} lineHeight hauteur de ligne
 */
CanvasRenderingContext2D.prototype.wrapText = function (text, x, y, maxWidth, lineHeight) {
    var lines = text.split("\n");

    for (var i = 0; i < lines.length; i++) {

        var words = lines[i].split(' ');
        var line = '';

        for (var n = 0; n < words.length; n++) {
            var testLine = line + words[n] + ' ';
            var metrics = this.measureText(testLine);
            var testWidth = metrics.width;
            if (testWidth > maxWidth && n > 0) {
                this.fillText(line, x, y);
                line = words[n] + ' ';
                y += lineHeight;
            }
            else {
                line = testLine;
            }
        }

        this.strokeText(line, x, y);
        y += lineHeight;
    }
};

function getWidth() {
    return window.innerWidth * RATIO_WIDTH;
}

function getHeight() {
    return window.innerHeight * RATIO_HEIGHT;
}

function getMargin() {
    return window.innerWidth * (1-RATIO_WIDTH)/2;
}

function wrapTexte(content) {
    return '<div><div class="content">'+content+'</div></div>';
}

var devMode = false;

function delay(time) {
    time = time || DEFAULT_DELAY;
    time = (devMode) ? 0 : time;

    return new Promise(function(resolve) {
        setTimeout(resolve, time);
    });
}