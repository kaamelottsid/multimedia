function sceneTen(app) {
    var percevalTarverne = '<div>' +
                            '<canvas id="c15" width="500" height="500" class="calque"></canvas>' +
                            '<img src="assets/images/perceval-graal.png" width="100%" style="max-height:220px">' +
                      '</div>',

        percevalGraalPerdu = '<div>' +
                                '<canvas id="c16" width="500" height="500" class="calque"></canvas>' +
                                '<img src="assets/images/perceval-graal_perdu.png" width="100%" style="max-height:220px">' +
                              '</div>',

       retourChateau =  '<div>' +
                           '<canvas id="c17" width="500" height="500" class="calque"></canvas>' +
                           '<img src="assets/images/retour_chateau.jpg" width="100%" style="max-height:220px">' +
                        '</div>',

       fin = "Fin",

       remerciements = '<div style="background-image: url(assets/images/equipe_kaamelott.jpg);background-size: 100% 100%">' +
                            '<div id="remerciements">Merci à Kaamelott</div>' +
                       '</div>';


    app.definePage(32, percevalTarverne, percevalAction)
       .definePage(33, percevalGraalPerdu, percevalAction2)
       .definePage(34, retourChateau, arthurAction)
       .definePage(35, wrapTexte(fin))
       .definePage(36, remerciements);

    function percevalAction() {
        var calque =  document.getElementById("c15"),
            calqueContext = calque.getContext("2d");

        app.drawBubble(calqueContext, "Aujourd'hui, j'ai trouvé le Graal !!", 300, 100, 70, 180, 180);
    }

    function percevalAction2() {
        var calque =  document.getElementById("c16"),
            calqueContext = calque.getContext("2d");

        app.drawBubble(calqueContext, "Il y a tellement de coupes que ...\nOh non, j'ai oublié laquelle c'était !!"
                                        , 300, 100, 30, 180, 180, 110);
    }

    function arthurAction() {
        var calque =  document.getElementById("c17"),
            calqueContext = calque.getContext("2d");

        app.drawBubble(calqueContext, "Vous avez fait quoi ?!" , 200, 100, 260, 225, 220);
    }
}