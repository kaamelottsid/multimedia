function sceneOne(app) {

    var introduction =
        "Il y a très longtemps, dans le royaume de l'île de Bretagne, " +
        "un nouveau chevalier fit son apparition.",

        explication =
        "Notre jeune compagnon venait tout juste d'être adoubé et était sur le point " +
        "d'assister à sa première réunion de la table ronde ... en présence du Roi Arthur !<br><br>"+
        "Il se trouvait devant la grande porte du château et s'apprêta à toquer.",

        gate = '<div>' +
                    '<canvas id="c1" width="500" height="500" class="calque"></canvas>' +
                    '<div style="position:relative;">' +
                        '<img id="porte" src="assets/images/gate.png" width="80%">' +
                        '<div id="plaque"></div>'+
                        '<div id="eyes"></div>'+
                    '</div>' +
               '</div>';

    app.definePage(3, wrapTexte(introduction))
        .definePage(4, wrapTexte(explication))
        .definePage(5, gate, gateAction);


    function gateAction() {
        var $porte = $("#porte"),
            $plaque = $("#plaque"),
            calque =  document.getElementById("c1"),
            calqueContext = calque.getContext("2d");

        $porte.on('click', function() {
            createjs.Sound.play("KNOCK-KNOCK");

            $plaque.animate({
                left: "20%"
            }, function() {
                app.drawBubble(calqueContext, "Qui va là ?", 100, 260, 100, 250, 200);
                calque.style.zIndex = 3;
                launchCanvas($plaque.parent());
            });
        });
    }

    function launchCanvas($page) {
        var $videoContainer = $("#video-container");
        if($videoContainer.length != 0) return;

        $page.append('<div id="video-container">' +
                        '<video id="outputCamera" autoplay></video>' +
                        '<button id="capture">Capturer</button>' +
                        '<button id="validerCapture">Valider</button>' +
                    '</div>');

        var $videoContainer = $("#video-container"),
            video = document.getElementById("outputCamera"),
            context = document.getElementById("avatar").getContext("2d"),
            $avatarContainer = $('.avatar-container');

        $videoContainer.css({
            "position": "absolute",
            "z-index": 3,
            "bottom": "8%",
            "left": "29%",
            "width": "40%",
            "height": "30%"
        });

        $(video).css("width", "100%");

        var imageSet = false;

        $videoContainer.find("button#capture").on('click', function() {
            imageSet = true;
            context.drawImage(video, 0, 0, 160, 120);
            $avatarContainer.addClass('filled');
        });

        var streamCamera = null;

        $videoContainer.find("button#validerCapture").on('click', function() {
            if(!imageSet) return;

            video.pause();
            video.src = "";
            streamCamera.getTracks()[0].stop();

            $videoContainer.hide();

            app.turnToNextPage();
        });

         navigator.webkitGetUserMedia({video: true}, function(stream) {
             streamCamera = stream;
             video.src =  window.webkitURL.createObjectURL(stream);
             video.play();
        }, function(e) { console.error(e); });
    }
}