function sceneFive(app) {

    var miseEnRoute = "Le jeune chevalier et Perceval se mirent alors en route vers le mystérieux village de Sarre ..."

        vueVieuxDehors = '<div style="background-image: url(assets/images/old_house.jpg);background-size: 100% 100%">' +
                            '<img style="position:absolute; left: 18%; bottom: 0" src="assets/images/old_man_half.png">' +
                            '<canvas id="c11" width="500" height="500" class="calque"></canvas>' +
                            '<div class="microphone">'+
                                '<div class="hint--top" data-hint="Utiliser le microphone"><img id="sayPassword" src="assets/images/mic-icon.png"></div>' +
                            '</div>' +
                          '</div>';

    app.definePage(20, wrapTexte(miseEnRoute))
       .definePage(21, vueVieuxDehors, vieuxAction);

    function vieuxAction() {
        var calque =  document.getElementById("c11"),
            calqueContext = calque.getContext("2d");

        var $mic = $("#sayPassword");

        if($mic.is(':visible')) {
            app.drawBubble(calqueContext, "Pour pouvoir entrer,\nle mot de passe vous devez prononcer."
                                , 320, 60, 50, 210, 220, 110);
        }

        $mic.on('click', function() {
            var transcription = '';

            var recognition = new webkitSpeechRecognition();
            recognition.lang = "fr-FR";

            recognition.onstart = function() {
                console.info("Début retranscription");
                reinit(calqueContext);
                app.drawBubble(calqueContext, "Je suis tout ouïe, mon ami.", 320, 60, 50, 210, 220);
            };

            recognition.onresult = function(event) {
                if (typeof(event.results) == 'undefined') {
                      recognition.onend = null;
                      recognition.stop();
                      return;
                }

                for (var i = event.resultIndex; i < event.results.length; ++i) {
                    transcription += event.results[i][0].transcript;
                }

                recognition.stop();
              };

              recognition.onend = function() {
                  reinit(calqueContext);

                  if(transcription == "Apple") {
                      app.drawBubble(calqueContext, "La bonne réponse, en effet, vous avez.", 320, 60, 50, 210, 220);
                      $mic.hide();

                      setTimeout(function () {
                          app.turnToNextPage();
                      }, 1000);
                  } else {
                      app.drawBubble(calqueContext, "Vous vous méprenez, jeune chevalier.", 320, 60, 50, 210, 220);
                  }

                  console.info("Fin retranscription");
              };

              recognition.start();
        });
    }

    function reinit(context) {
        context.clearRect(0, 0, 500, 500);
    }
}