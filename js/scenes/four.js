function sceneFour(app) {
    var allerBibliotheque = "Notre jeune héros décida alors de se rendre à la bibliothèque du château " +
        "afin de trouver une carte pour déterminer le chemin le plus court pour se rendre à Sare, en Aquitaine. " +
        "Dans le but de préparer son expédition, il chercha également des ouvrages pouvant contenir des informations sur ce mystérieux village ...",

        pereBlaiseTalking = '<div>' +
                                '<canvas id="c9" width="500" height="500" class="calque"></canvas>' +
                                '<img src="assets/images/pere_blaise.jpg" width="100%" style="max-height:220px">' +
                            '</div>',

       vueBibliotheque =    '<div style="background-image: url(assets/images/bibliotheque.jpg);background-size: 100% 100%">' +
                               '<canvas id="c10" width="500" height="500" class="calque"></canvas>' +
                           '</div>',

       vueLivres = '<div>' +
                       '<div class="carved" style="position: absolute; z-index: 2; top: 27%; right: 3%; line-height:35px; width:50px; transform: rotate3d(1, 0, -0.122, 60deg)">Les gobelins craignent le feu</div>' +
                       '<img src="assets/images/books.jpg" width="100%" style="max-height:220px">' +
                   '</div>',

       apresRecherches = "Après de longues heures de recherche dans cette immense bibliothèque, notre jeune chevalier finit par trouver une carte du Royaume de Bretagne. " +
                         "Cette carte allait lui permettre de déterminer le chemin le plus efficace pour se rendre en Aquitaine.",

       carteBretagne = '<div style="background-image: url(assets/images/carte_royaume_bretagne.jpg);background-size: 100% 100%">' +
                          '<svg id="carte_paths" width="500px" height="500px" version="1.1" xmlns="http://www.w3.org/2000/svg">' +
                            '<path class="wrong" d="M 245 300 L 212 378 L 230 450" stroke="#1771D9" fill="transparent" data-name="vannes"/>' +
                            '<path class="wrong" d="M 245 295 L 270 220 L 310 330 L 310 450 L 230 450" stroke="#654140" fill="transparent" data-name="carmelide"/>' +
                            '<path class="right" d="M 245 300 L 290 400 L 230 450" stroke="#7C6295" fill="transparent" data-name="bon"/>' +
                            '<filter id="drop-shadow">' +
                              '<feGaussianBlur in="SourceAlpha" stdDeviation="4"/>' +
                              '<feOffset dx="0" dy="0" result="offsetblur"/>' +
                              '<feFlood flood-color="rgba(255,255,255,0.8)"/>' +
                              '<feComposite in2="offsetblur" operator="in"/>' +
                              '<feMerge>' +
                                '<feMergeNode/>' +
                                '<feMergeNode in="SourceGraphic"/>' +
                              '</feMerge>' +
                            '</filter>' +
                          '</svg>' +
                       '</div>';

    app.definePage(14, wrapTexte(allerBibliotheque))
       .definePage(15, pereBlaiseTalking, pereBlaiseAction)
       .definePage(16, vueBibliotheque, bibliothequeAction)
       .definePage(17, vueLivres)
       .definePage(18, wrapTexte(apresRecherches))
       .definePage(19, carteBretagne, carteAction);

    function pereBlaiseAction() {
        var calque =  document.getElementById("c9"),
            calqueContext = calque.getContext("2d");

            app.drawBubble(calqueContext, "Des livres sur le village de Sarre ?\n" +
                        "Rien ne me vient à l'esprit.\n" +
                        "Vous n'avez qu'à chercher par vous-même.\n" +
                        "La bibliothèque, on en fait vite le tour."
                                , 375, 5, 250, 240, 230, 190);
    }

    function bibliothequeAction() {
        var calque =  document.getElementById("c10"),
            calqueContext = calque.getContext("2d");

            delay(500).then(function() {
                app.drawBubble(calqueContext, "Je rigolais.\nTu vas galérer !", 160, 100, 250, 10, 320, 100);
            });
    }

    var phrases = {
        'carmelide': "Oh non. Si on passe par là, va encore falloir se taper Léodagan et ses anecdotes de torture.",
        'vannes': "Si on passe par là, à tous les coups, la famille de Karadoc va nous inviter à becter et ça va encore durer une plombe.",
        'bon': "Et ben voilà. Là, on est bien !"
    }

    function carteAction() {
        var $paths = $("#carte_paths").find("path");

        $paths.on('click', function() {
            if(this.classList.contains('selected')) return;
            this.classList.add('selected');

            var name = $(this).data('name');
            var fluxParole = prononcer(phrases[name]);

            if(this.classList.contains('right')) {
                fluxParole.then(function() {
                    app.turnToNextPage();
                });
            }
        });
    }

    function prononcer(texte) {
        var msg = new SpeechSynthesisUtterance();
        msg.text = texte;
        msg.pitch = 0.01;
        msg.rate = 1.2;
        msg.lang = 'fr-FR';

        window.speechSynthesis.speak(msg);

        return new Promise(function(resolve) {
            msg.onend = resolve;
        });
    }
}