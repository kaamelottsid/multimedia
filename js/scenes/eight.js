function sceneEight(app) {
    var percevalPiege = "Nos héros s'avancèrent dans cette mystérieuse grotte, quand soudain, Perceval mis son pied sur une pierre qui activa un piège.<br>" +
                        "Des flèches franchissaient la pièce dans tous les sens. Le jeune chevalier dù se baisser afin d'éviter les projectiles mortels. C'est à ce moment là " +
                        "qu'il vit quelque chose d'étrange au sol.",

        vueInterieurGrotte = '<div id="grotte" style="background-image: url(assets/images/inside_grotte.jpg);background-size: 100% 100%">' +
                                '<canvas id="c14" width="500" height="500" class="calque"></canvas>' +
                                '<div id="lyrics">'+
                                    'Mon petit oiseau<br>' +
                                    'A pris sa volée<br>' +
                                    'A pris sa, à la volette<br>' +
                                    'A pris sa volée'+
                                '</div>' +
                                '<div class="microphone">'+
                                    '<div class="hint--top" data-hint="Utiliser le microphone"><img id="sayLyrics" src="assets/images/mic-icon.png"></div>' +
                                '</div>' +
                             '</div>';

    app.definePage(28, wrapTexte(percevalPiege))
       .definePage(29, vueInterieurGrotte, grotteAction);

    var timeOutId = null,
        piegeDesactive = false,
        lyrcis = "mon petit oiseau a pris sa volée a pris sa à la volette a pris sa volée";

    function grotteAction() {
        var calque =  document.getElementById("c14"),
            calqueContext = calque.getContext("2d");

        var $grotte = $("#grotte"),
            $mic = $grotte.find("#sayLyrics");

        if(!piegeDesactive) startFleches($grotte);

        $mic.on('click', function() {
            var transcription = '';

            var recognition = new webkitSpeechRecognition();
            recognition.lang = "fr-FR";
            recognition.continuous = true;

            recognition.onstart = function() {
                console.info("Début retranscription");
            };

            recognition.onresult = function(event) {
                if (typeof(event.results) == 'undefined') {
                      recognition.onend = null;
                      recognition.stop();
                      return;
                }

                for (var i = event.resultIndex; i < event.results.length; ++i) {
                    transcription += event.results[i][0].transcript;
                }

                recognition.stop();
              };

              recognition.onend = function() {
                  console.log(transcription);

                  if(transcription == lyrcis) {
                      piegeDesactive = true;
                      stopFleches();
                      app.drawBubble(calqueContext, "Tu nous as sauvé !", 200, 120, 80, 30, 120);

                      setTimeout(function() {
                          app.turnToNextPage();
                      }, 1500);
                  }

                  console.info("Fin retranscription");
              };

              recognition.start();
        });
    }

    function startFleches($grotte) {
        timeOutId = setTimeout(function() {
            startFleches($grotte);
        }, 200);

        var $el;

        $el = $('<img class="fleche" src="assets/images/fleche.png">');

        var isReversed = ((parseInt(Math.random() * 10)) % 2 == 0);
        var side = isReversed ? 'right' : 'left';

        $grotte.append($el);
        $el.css('top', 10 + parseInt(Math.random() * 300));

        if(isReversed) $el.addClass('reverse');

        var obj = {};
        obj[side] = '1000px';

        $el.animate(obj, 1000);
    }

    function stopFleches() {
        clearTimeout(timeOutId);
    }
}