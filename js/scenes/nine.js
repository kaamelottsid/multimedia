function sceneNine(app) {
    var vueInterieurGrotte = '<div style="background-image: url(assets/images/inside_grotte.jpg);background-size: 100% 100%">' +
                                '<img id="safe" src="assets/images/trunk.png">' +
                                '<img id="graal-safe" src="assets/images/graal_cut.png">' +
                             '</div>',

        texteFestoiement = "Notre jeune chevalier, en dépit de tous dangers, parvint à trouver le seul et unique Graal.<br>" +
                           "En l'honneur de cette merveilleuse nouvelle, Perceval proposa de se rendre à la taverne afin de festoyer cela.";

     app.definePage(30, vueInterieurGrotte)
        .definePage(31, wrapTexte(texteFestoiement));
}