function sceneSix(app) {
    var vueVieuxDedans = '<div style="background-image: url(assets/images/old_house_inside.jpg);background-size: 100% 100%">' +
                            '<img style="position:absolute; left: 18%; bottom: 0" src="assets/images/old_man_half.png">' +
                            '<canvas id="c12" width="500" height="500" class="calque"></canvas>' +
                         '</div>',

       vueCles = '<div style="background-image: url(assets/images/old_house_inside.jpg);background-size: 100% 100%">'+
                    '<img class="wrong key" style="bottom:24.5%;left:39%" src="assets/images/first_key.png">'+
                    '<img class="right key" style="bottom:24%;right:33%" src="assets/images/second_key.png">'+
                    '<img class="wrong key" style="bottom:7%;left:25%" src="assets/images/third_key.png">'+
                 '</div>',

      explicationCle = "Cette clé permetta d'ouvrir un coffre appartenant à l'étrange vieillard. Ce coffre contenait une carte " +
                       "menant à une grotte qui serait le foyer du Graal.";

    app.definePage(22, vueVieuxDedans, vieuxAction)
       .definePage(23, vueCles, clesAction)
       .definePage(24, wrapTexte(explicationCle));

    function vieuxAction() {
        var calque =  document.getElementById("c12"),
            calqueContext = calque.getContext("2d");

        app.drawBubble(calqueContext, "Pour obtenir La coupe sacrée,\nla clé vous devez trouver."
                                , 320, 60, 50, 210, 220, 110);
    }

    var fluxParole;

    function clesAction() {
        var $keys = $("img.key");

        $keys.on('click', function() {
            if($(this).hasClass('right')) {
                fluxParole = prononcer("Félicitations. La véritable clé vous avez trouvé.");
                fluxParole.then(function() {
                    app.turnToNextPage();
                });
            } else {
                prononcer("Il ne s'agit pas de cette clé là.");
                $(this).hide();
            }
        });
    }

    function prononcer(texte) {
        var msg = new SpeechSynthesisUtterance();
        msg.text = texte;
        msg.pitch = 0.01;
        msg.rate = 0.8;
        msg.lang = 'fr-FR';

        window.speechSynthesis.speak(msg);

        return new Promise(function(resolve) {
            msg.onend = resolve;
        });
    }

}
