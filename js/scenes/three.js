function sceneThree(app) {
    var voirPerceval = "Le jeune chevalier fut à la fois excité et terrifié à l'idée d'effectuer sa première mission. " +
        "Il souhaitait impressioner le roi en faisant preuve d'initative et décida d'interroger Perceval pour obtenir " +
        "plus d'informations sur son aventure.",

        percevalDebrief = '<div>' +
                                '<canvas id="c8" width="500" height="500" class="calque"></canvas>' +
                                '<img src="assets/images/perceval-karadoc.jpg" width="100%" style="max-height:220px">' +
                          '</div>';

    app.definePage(12, wrapTexte(voirPerceval))
       .definePage(13, percevalDebrief, percevalAction);


    function percevalAction() {
        var calque =  document.getElementById("c8"),
            calqueContext = calque.getContext("2d");

        app.drawBubble(calqueContext, "Plus de détails ? Voyons voir ...\n"+
                             "Ben, c'était juste un vieux pas jojo\n" +
                             "qui n'arrêtait pas de dire \"Apple\".\n" +
                             "Encore une insulte de vieux à tous les coups"
                            , 375, 5, 250, 100, 230, 190);
    }
}