function sceneTwo(app) {
    var tableRonde = '<div>' +
                        '<canvas id="c2" width="500" height="500" class="calque"></canvas>' +
                        '<img src="assets/images/table_ronde.jpg" width="100%" style="max-height:220px">' +
                      '</div>',

        percevalTalking = '<div>' +
                            '<canvas id="c3" width="500" height="500" class="calque"></canvas>' +
                            '<img src="assets/images/perceval-karadoc_armures.jpg" width="100%" style="max-height:220px">' +
                          '</div>',

        percevalTalking2 =  '<div>' +
                                '<canvas id="c4" width="500" height="500" class="calque"></canvas>' +
                                '<img src="assets/images/table_ronde_2.jpg" width="100%" style="max-height:220px">' +
                            '</div>',

        percevalTalking3 =  '<div>' +
                                '<canvas id="c5" width="500" height="500" class="calque"></canvas>' +
                                '<img src="assets/images/table_ronde_2.jpg" width="100%" style="max-height:220px">' +
                            '</div>',
        arthurTalking =  '<div>' +
                                '<canvas id="c6" width="500" height="500" class="calque"></canvas>' +
                                '<img src="assets/images/table_ronde_3.jpg" width="100%" style="max-height:220px">' +
                            '</div>',
        arthurTalking2 =  '<div>' +
                                '<canvas id="c7" width="500" height="500" class="calque"></canvas>' +
                                '<img src="assets/images/table_ronde_4.jpg" width="100%" style="max-height:220px">' +
                            '</div>';

    app.definePage(6, tableRonde, tableRondeAction)
        .definePage(7, percevalTalking, percevalAction)
        .definePage(8, percevalTalking2, percevalAction2)
        .definePage(9, percevalTalking3, percevalAction3)
        .definePage(10, arthurTalking, arthurAction)
        .definePage(11, arthurTalking2, arthurAction2);

    var flux;

    function tableRondeAction() {
        var calque =  document.getElementById("c2"),
            calqueContext = calque.getContext("2d");

        flux = delay(500).then(function() {
            app.drawBubble(calqueContext, "Il y avait un vieux, je vous dis !", 280, 5, 100, 100, 200);
            return delay();
        }).then(function() {
            app.drawBubble(calqueContext, "Oh non, ça va pas recommencer !", 300, 50, 260, 215, 240);
            return delay();
        });
    }

    function percevalAction() {
        var calque =  document.getElementById("c3"),
            calqueContext = calque.getContext("2d");

        flux.then(function() {
            app.drawBubble(calqueContext, "Si si ! Même que ce coup-ci,\nil a parlé du Graal.", 250, 80, 280, 130, 260, 110);
            return delay();
        }).then(function() {
            app.drawBubble(calqueContext, "Sans déc ?!", 120, 150, 120, 260, 220);
        });
    }

    function percevalAction2() {
        var calque =  document.getElementById("c4"),
            calqueContext = calque.getContext("2d");

        flux = delay(500).then(function() {
            app.drawBubble(calqueContext, "Il vient et il me dit qu'une carte\nmenant au Graal se trouve en Aquitaine !",
                                          360, 5, 80, 70, 210, 100);
            return delay();
        }).then(function() {
            app.drawBubble(calqueContext, "Où ça en Aquitaine ?", 210, 80, 260, 205, 240);
            return delay();
        });
    }

    function percevalAction3() {
        var calque =  document.getElementById("c5"),
            calqueContext = calque.getContext("2d");

        flux.then(function() {
            app.drawBubble(calqueContext, "Dans un patelin appelé Sare, Sire !\nC'est son frangin qui aurait la carte.", 320, 5, 80, 70, 210, 100);
            return delay();
        }).then(function() {
            app.drawBubble(calqueContext, "Pourquoi il faut toujours qu'il y ait\nun vieux dans vos histoires ?", 320, 40, 260, 205, 240, 100);
        });
    }

    function arthurAction() {
        var calque =  document.getElementById("c6"),
            calqueContext = calque.getContext("2d");

            flux = delay(500).then(function() {
                app.drawBubble(calqueContext, "Bon et bien, même si j'ai des gros doutes\nquant à la véracité de ces informations,\n" +
                                              "je me vois dans l'obligation de vous\nenvoyer en mission de reconnaissance.\n" +
                                              "Mais pas tout seul ..."
                                    , 370, 5, 260, 205, 240, 220);
                return delay();
            });
    }

    function arthurAction2() {
        var calque =  document.getElementById("c7"),
            calqueContext = calque.getContext("2d");

            flux.then(function() {
                app.drawBubble(calqueContext, "Perceval, vous irez avec le p'tit nouveau !", 370, 10, 240, 180, 220);
            });
    }
}