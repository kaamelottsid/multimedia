function sceneSeven(app) {
    var introVoyageGrotte = "Notre jeune chevalier et Perceval se mirent alors en route pour découvrir cette mystérieuse grotte remplie de promesses.<br>"+
                            "Le chemin était périlleux mais nos héros ne pouvaient reculer devant une telle opportunité.",

        introEntreeGrotte = "Une fois arrivée à l'entrée, nos aventuriers virent au fond de la grotte des yeux jaunes luisants.<br>"+
                           "Perceval savait de quoi il s'agissait : des gobelins. Il se rappelait qu'un des éléments de la nature pouvait vaincre ces créatures " +
                           "mais n'arrivait pas à se rappeler duquel.",

        vueEntreeGrotte = '<div style="background-image: url(assets/images/grotte.jpg);background-size: 100% 100%">' +
                            '<img class="goblin-eyes" style="top:32%;left:37%" src="assets/images/yellow_eyes.png">' +
                            '<img class="goblin-eyes" style="top:38%;left:40%" src="assets/images/yellow_eyes.png">' +
                            '<img class="goblin-eyes" style="top:33%;left:52%" src="assets/images/yellow_eyes.png">' +
                            '<img class="goblin-eyes" style="top:44%;left:54%" src="assets/images/yellow_eyes.png">' +
                            '<img class="goblin-eyes" style="top:44%;left:38%" src="assets/images/yellow_eyes.png">' +
                            '<canvas id="c13" width="500" height="500" class="calque"></canvas>' +
                            '<div id="elements">'+
                                '<div class="hint--top" data-hint="Utiliser l\'eau"><div class="element water"><img src="assets/images/water-icon.png"></div></div>' +
                                '<div class="hint--top" data-hint="Utiliser le feu"><div class="element fire"><img src="assets/images/fire-icon.png"></div></div>' +
                                '<div class="hint--top" data-hint="Utiliser la foudre"><div class="element thunder"><img src="assets/images/thunder-icon.png"></div></div>' +
                                '<div class="hint--top" data-hint="Utiliser des cailloux"><div class="element rock"><img src="assets/images/rock-icon.png"></div></div>' +
                            '</div>'
                    '</div>';

    app.definePage(25, wrapTexte(introVoyageGrotte))
       .definePage(26, wrapTexte(introEntreeGrotte))
       .definePage(27, vueEntreeGrotte, entreeGrotteAction);

    var timeOutId, gobelinsVaincus = false;

    function entreeGrotteAction() {
        if(timeOutId) clearTimeout(timeOutId);

        var calque =  document.getElementById("c13"),
            calqueContext = calque.getContext("2d");

        var $elements = $("#elements").find(".element"),
            $eyes = $("img.goblin-eyes");

        $elements.on('click', function() {
            if($(this).hasClass('fire')) {
                var $fireBurning = $('<div id="fire-burning" />').appendTo($("#elements").parent());
                $fireBurning.fadeIn();
                createjs.Sound.play("FIRE");

                delay(1800).then(function() {
                    $fireBurning.fadeOut(function() {
                        $eyes.fadeOut();
                        clearTimeout(timeOutId);
                        $elements.hide();
                        gobelinsVaincus = true;
                    });

                    return delay(0);
                }).then(function() {
                    app.drawBubble(calqueContext, "Ça a fonctionné !", 200, 120, 300, 30, 320);

                    return delay(2000);
                }).then(function() {
                    app.turnToNextPage();
                });
            }
        });

        (function updateEyes() {
            if(gobelinsVaincus) return;

            timeOutId = setTimeout(updateEyes, 800);

            $eyes.each(function() {
                var random = parseInt(Math.random() * 10);
                (random % 2 == 0) ? $(this).fadeOut() : $(this).fadeIn();
            });
        })();
    }
}